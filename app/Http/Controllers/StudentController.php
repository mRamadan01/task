<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Student;
use App\School;
use Session;
use Excel;
use File;
use Validator;
// use PDF;
use PDFAnony\TCPDF\Facades\AnonyPDF as PDF;

class StudentController extends Controller
{
    public function importfile()
    {
    	return view('welcome');
    }
    public function uploadexcel(Request $request)
    {

        //validate the xls file
        $request->validate([

            'import_file' => 'required'

        ]);
        if($request->hasFile('import_file')){

            $extension = File::extension($request->import_file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->import_file->getRealPath();
                //Excel::selectSheets('Sheet2')->load();
                $school_data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {
                $reader->skipRows(3)->skipColumns(2)->ignoreEmpty()->noHeading()->toArray();          
                })->all();
                $school_exist = School::where('school_name','=',$school_data[0][0])->exists();
                
                if(!empty($school_data) && $school_data->count() && $school_exist == false){

                        $school = new School;
                        $school->school_name = $school_data[0][0];
                        $school->school_number = $school_data[1][0];
                        $school->save();
                }
                    if($school_exist){
                        Session::flash('error', 'اسم المدرسة موجود مسبقا');
                        return back();
                    }                

               $data =  Excel::selectSheetsByIndex(1)->load($path, function($reader) {
               	$reader->skipRows(4)->ignoreEmpty()->noHeading();          
                })->all();         
                $count_student = 0;    
                //dd($data->count());
                if(!empty($data) && $data->count() && isset($school)){                
                foreach($data->toArray() as $key => $value)
                      {
                        if (!empty($value[4])) {
                        $student_exist = Student::where('student_id',$value[4])->exists();
                            if (isset($value[0]) && $student_exist ==false) {
                            $student = new Student;
                            $student->mobile = $value[0];
                            $student->class_number = $value[1];
                            $student->row_number = $value[2];
                            $student->name = $value[3];
                            $student->student_id = $value[4];
                            $student->school_id = $school->id;
                            $student->save();
                            $count_student++;
                            }                        
                        }
                        
                        
                      }

                    if(!empty($student)){
                        Session::flash('success', 'Your Data has successfully imported');
                        return back()->with('count',$count_student);
                    }
                }
                return back();
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
            }

    }
    public function uploadexcelAPI(Request $request)
    {

        //validate the xls file
        $request->validate([

            'import_file' => 'required'

        ]);
        if($request->hasFile('import_file')){

            $extension = File::extension($request->import_file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
  
                $path = $request->import_file->getRealPath();
                //Excel::selectSheets('Sheet2')->load();
                $school_data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {
                $reader->skipRows(3)->skipColumns(2)->ignoreEmpty()->noHeading()->toArray();          
                })->all();
                $school_exist = School::where('school_name','=',$school_data[0][0])->exists();
                
                if(!empty($school_data) && $school_data->count() && $school_exist == false){

                        $school = new School;
                        $school->school_name = $school_data[0][0];
                        $school->school_number = $school_data[1][0];
                        $school->save();
                }
                    if($school_exist){
                        $response['status'] = -1;
                        $response['message'] = 'اسم المدرسة موجود مسبقا';
                        return response()->json($response);
                    }                

               $data =  Excel::selectSheetsByIndex(1)->load($path, function($reader) {
                $reader->skipRows(4)->ignoreEmpty()->noHeading();          
                })->all();         
                $count_student = 0;    
                //dd($data->count());
                if(!empty($data) && $data->count() && isset($school)){                
                foreach($data->toArray() as $key => $value)
                      {
                        if (!empty($value[4])) {
                        $student_exist = Student::where('student_id',$value[4])->exists();
                            if (isset($value[0]) && $student_exist ==false) {
                            $student = new Student;
                            $student->mobile = $value[0];
                            $student->class_number = $value[1];
                            $student->row_number = $value[2];
                            $student->name = $value[3];
                            $student->student_id = $value[4];
                            $student->school_id = $school->id;
                            $student->save();
                            $count_student++;
                            }                        
                        }
                        
                        
                      }

                    if(!empty($student)){
                        $response['status'] = 1;
                        $response['message'] = 'Your Data has successfully imported';
                        $response['count'] = $count_student.'Has been added';
                        return response()->json($response); 
                    }
                }
             	$response['status'] = -1;
            	$response['message'] = 'CHECK FILE AGAIN';
				return response()->json($response); 
                
            }else {
             	$response['status'] = -1;
            	$response['message'] = 'File extension ERROR ,Please upload a valid xls/csv file..!!';
				return response()->json($response);             	
                
            }
            }

    }    
    public function get_student_by_student_id(Request $request)
    {
    	$student_id = $request->student_id;

    	$student = Student::where('student_id',$student_id)->first();

    	if ($student) {
    		return view('student_data')->with('student',$student);
    		
    	}else{
                Session::flash('error_id', 'لا يوجد طالب يحمل هذا الرقم ');
                return back();    		
    	}

    }
    public function print_pdf($id)
    {
    	$data = Student::find($id);
		$html = view('pdf.student', compact('data'))->render(); // file render
		 $pdfarr = [
		'title'=>'Student Data',
		'data'=>$html, // render file blade with content html
		'header'=>['show'=>false], // header content
		'footer'=>['show'=>false], // Footer content
		'font'=>'dejavusans', //  dejavusans, aefurat ,aealarabiya ,times
		'font-size'=>10, // font-size 
		'text'=>'', //Write
		'rtl'=>true, //true or false 
		'filename'=>'student.pdf', // filename example - invoice.pdf
		'display'=>'stream', // stream , download , print
	];

 		 return 	PDF::HTML($pdfarr);

    }
}
