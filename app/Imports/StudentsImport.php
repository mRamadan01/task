<?php

namespace App\Imports;

use App\Student;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class StudentsImport implements WithMultipleSheets
{
        use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'Sheet1' => new FirstSheetImport(),
            'Sheet2' => new SecondSheetImport(),
            'Sheet3' => new ThirdSheetImport(),
        ];
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    //     public function sheets(): array
    // {

    //     return [
    //         'Sheet1' => $this,
    //         'Sheet1' => $this
    //     ];
    // }
    // public function model(array $row)
    // {
    //     dd($row);
    //     return new Student([
            
    //     ]);
    // }
}
