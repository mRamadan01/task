<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }
}
