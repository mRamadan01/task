<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StudentController@importfile');
Route::post('/', 'StudentController@uploadexcel')->name('uploadFile');
Route::post('/student', 'StudentController@get_student_by_student_id')->name('findStudent');
Route::post('/pdf/{id}', 'StudentController@print_pdf')->name('printPDF');
