<html lang="en">

<head>

    <title>Upload excel and Find record</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >

</head>

<body>

    <div class="container">

        <div class="panel panel-default">

          <div class="panel-heading">

          <h1>Upload excel and Find record</h1>

          </div>

          <div class="panel-body">


            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('uploadFile') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

                @csrf

 

                @if (Session::has('error'))

                    <div class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <ul>

                                <li>{{ Session::get('error') }}</li>

                        </ul>

                    </div>

                @endif

 

                @if (Session::has('success'))

                    <div class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <p>{{ Session::get('success') }}</p>

                    </div>

                @endif
                @if (Session::has('count'))

                    <div class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <p> طالب{{ Session::get('count') }} تم اضافه</p>

                    </div>

                @endif
 

                <input type="file" name="import_file" />

                <button class="btn btn-primary">Import File</button>

            </form>
            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('findStudent') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

                @csrf
<div class="alert alert-info">
  <label>بحث عن الطالب برقم الجلوس</label>
<br>
</div>
                @if (Session::has('error_id'))

                    <div class="alert alert-danger">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                        <ul>

                                <li>{{ Session::get('error_id') }}</li>

                        </ul>

                    </div>

                @endif
 

                <input type="text" name="student_id" />
                <label>رقم الجلوس</label>
                <br><br>    

                <button class="btn btn-primary">بحث</button>

            </form>
 

          </div>

        </div>

    </div>

</body>

</html>